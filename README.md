# testNextjs



## Main Version: Details

_This version has Docket functionality, uses persinal IPFS libraries, Blockchain is optional (see version 2 to test Blockchain)._


## Edits needed to render IPFS

Before you select a file, edit [pages/api/uploadToIPFS.js L36](https://gitlab.com/colettekaya94/testnextjs/-/edit/version2/README.md) to include your own API token.

You can obtain one by signing up / logging in to web3.storage, then selecting Account->Create an API token, choosing a name, and copying the generated token.

## Steps for running

The dependencies have been updated in package.json, so run with:

```
npm install 

npm run dev
```

In case problems with install, try:

```
npm install three

npm install tailwindcss@latest postcss@latest autoprefixer@latest

npm install react-icons --save
```

Then go to the UI at localhost:3000,

Once you select a file from your local PC and hit SUBMIT, the terminal prints out a CID for your file, and the file will show up inside your pages/uploads folder.

You can check its availability at IPFS by going to [web3.storage](https://web3.storage/docs/how-tos/retrieve/) and redirecting to your own w3 link as described. For example, the earthmap.jpg will show up by replacing the enter-generated-cid-here by its unique one we've generated [https://emter-generated-cid-here.ipfs.w3s.link/uploads](https://bafybeicatbbwsp6yf3ku6mz52u5qnojpfshhslacrijohno5xq755dwdm4.ipfs.w3s.link/uploads).

Done!!

## Previous versions

See branches _Version1_ and _Version2_. 

**_Version1_** was build to test visual functionality, has not IPFS or saving capabilities.

**_Version2_** was built to add IPFS capability and to test Blockchain but was depreciated due to fees being charged at each deployment (i.e., transfer). You can still test it.

## Next Editing Ideas
- [ ] Add rating functionality to show how useful User thinks the documents they view were.
- [ ] Write ML to suggest similar files to view to User.

## Bug fixes that may be needed

Docker files may need fixing.

The original project has environmental setup files that this version is missing, IPFS token setup is done by hand for now.

Dev notes: The app is functioning well at local host 3000; was developed both in local environment and Vercel. Vercel started giving build errors, may need to readjust local settings and reupload to Git - may also solve Docker build problem, which seems to be complaining about the same thing (build command).
