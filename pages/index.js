import React, { useState, useEffect } from 'react';
import Earth from '../components/Earth';
import { DiReact } from "react-icons/di";
import Link from 'next/link';

function App( cid ) {
  const [file, setFile] = useState(null);
  const [fileName, setFileName] = useState("");
  const [result, setResult] = useState("");
  const [isLoading, setIsLoading] = useState(false); // New state for loading state

  useEffect(() => {}, []);

  const handleChange = (e) => {
    if (e.target.name === "filename") {
      setFileName(e.target.value);
    }
    if (e.target.name === "file") {
      setFile(e.target.files[0]);
    }
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      setIsLoading(true); // Set loading state to true

      var formData = new FormData();
      formData.append("filename", fileName);
      formData.append("file", file);

      const res = await fetch("/api/uploadToIPFS", {
        method: "POST",
        body: formData
      });

      if (!res.ok){
        throw new Error("Network response is not OK :/");
      }
      const data = await res.json();
      setResult(data.message);
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false); // Set loading state to false after the request is completed
    }
  }

  return (
    <div className="container">
      <header className="header">
        <h1 className="text-5xl text-green-300"> <DiReact/> <span> Store IPFS hash on Blockchain</span> <DiReact/> </h1>
      </header>
      <form onSubmit={handleSubmit}>
        <label className="label"> Enter your unique file-name: </label>
        <input type="text" name="filename" value={fileName} onChange={handleChange} className="button"></input>
        <br/>
        <input type="file" name="file" onChange={handleChange} className="file-button"></input>
        <br/>
        <button type="submit" className={`submit-button ${isLoading ? 'loading' : ''}`}>
          {isLoading ? 'LOADING...' : 'Submit'}
        </button>
      </form>
      <div className="mt-4">
        <Link href="/uploaded">
          <div className="text-orange-500 italic cursor-pointer">Go to Archive to see what has been saved..</div>
        </Link>
      </div>
      <div className="mt-4">
        <Link href={`https://ipfs.io/ipfs/`}>
          <div className="text-pink-500 italic cursor-pointer">Alternatively, you can insert a CID at the end of this web3 link -https://ipfs.io/ipfs/- to view the uploads at the IPFS server</div>
        </Link>
      </div>
      {result && <p className="result"> {result} </p>}
      <div style={{ width: '10vw', height: '10vh' }}>
        <Earth />
      </div>
    </div>
  );
}

export default App;
