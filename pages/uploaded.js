import React, { useState, useEffect } from 'react';
import Link from 'next/link';

export default function Uploaded() {
  const [files, setFiles] = useState([]);
  const [selectedFile, setSelectedFile] = useState('');
  const [fileContent, setFileContent] = useState('');
  const [fileType, setFileType] = useState(null); // Initialize fileType with null
  const [showFileContent, setShowFileContent] = useState(false); // Add state to control file content visibility

  useEffect(() => {
    // Fetch files from the server using the API route
    const fetchFiles = async () => {
      try {
        const response = await fetch('/api/uploads');
        const data = await response.json();
        setFiles(data.files);
      } catch (error) {
        console.error('Error fetching files:', error);
      }
    };

    fetchFiles();
  }, []);

  const handleFileSelect = (e) => {
    setSelectedFile(e.target.value);
  };

  const handleFileView = async () => {
    try {
      const response = await fetch(`/api/uploads/${selectedFile}`);
      const data = await response.text();
      setFileType(response.headers.get('Content-Type'));
      setFileContent(data);
      setShowFileContent(true); // Set showFileContent to true after clicking "View File"
    } catch (error) {
      console.error('Error fetching file content:', error);
    }
  };

  const renderFileContent = () => {
    if (fileType && selectedFile.endsWith('.txt')) {
      return <pre>{fileContent}</pre>;
    } else if (fileType && fileType.startsWith('image/')) {
      return <img src={`/api/uploads/${selectedFile}`} alt="Uploaded Image" />;
    } else {
      return <p>Cannot display this file type.</p>;
    }
  };

  return (
    <div>
      <h1 className="mt-2 text-2xl italic text-pink-400 border-green-300">Display the files uploaded to the IPFS Server & stored locally..</h1>
      {files.length > 0 ? (
        <div className="mt-4">
          <label htmlFor="fileSelect" className="font-bold">
            Select & Click "View File" to refresh:
          </label>
          <select
            id="fileSelect"
            value={selectedFile}
            onChange={handleFileSelect}
            className="mt-2 border bg-green-200 border-green-300 text-blue-800 rounded-md px-3 py-2"
          >
            <option value="">-- Select a file to display from Archive --</option>
            {files.map((file) => (
              <option key={file} value={file}>
                {file}
              </option>
            ))}
          </select>
          <button
            onClick={handleFileView}
            disabled={!selectedFile}
            className="mt-4 bg-green-500 hover:bg-green-600 text-blue-800 font-bold py-2 px-4 rounded disabled:bg-orange-300 disabled:cursor-not-allowed"
          >
            View File
          </button>

          {fileContent && (
            <div className="mt-4">
              <h2 className="text-xl font-bold">File Content:</h2>
              {renderFileContent()}
            </div>
          )}
        </div>
      ) : (
        <p>No files uploaded yet.</p>
      )}
      <div className="mt-4">
        <Link href="/">
          <div className="text-yellow-400 cursor-pointer">Go back home..</div>
        </Link>
      </div>
    </div>
  );
}
