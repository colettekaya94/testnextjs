import fs from 'fs';
import path from 'path';

export default function handler(req, res) {
  const uploadsDirectory = path.join(process.cwd(), 'pages', 'uploads');
  const files = fs.readdirSync(uploadsDirectory);

  res.status(200).json({ files });
}
