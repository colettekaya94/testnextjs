import { Web3Storage, getFilesFromPath } from 'web3.storage';
import formidable from 'formidable';
import path from 'path';
//import fs from 'fs';

export const config = {
  api: {
    bodyParser: false // disable built-in body parser
  }
};

function moveFileToServer(req) {
  return new Promise((resolve, reject) => {
    //*//const form = new formidable.IncomingForm();
    const options = {};
    options.uploadDir = path.join(process.cwd(), '/pages/uploads');
    options.filename = (name, ext, path, form) => {
        return path.originalFilename;
      };
      const form = formidable(options);
  
      form.parse(req, (err, fields, files) => {
        if (err) {
          console.error(err);
          reject('Something went wrong :/');
          return;
        }
        const uniqueFileName = fields.filename;
        const actualFileName = files.file.originalFilename;
  
        resolve({ uniqueFileName, actualFileName });

    //*//options.keepExtensions = true;
    //*//form.parse(req, async (err, fields, files) => {
    //*//  if (err) {
    //*//    console.error(err);
    //*//    reject('Something went wrong :/');
    //*//    return;
    //*//  }
    //*//  let uniqueFileName;
    //*//  let actualFileName;
    //*//  if (files.file) {
    //*//    // File is uploaded through the browser
    //*//    uniqueFileName = files.file.name;
    //*//    actualFileName = files.file.name;
    //*//    const filePath = files.file.path;
    //*//    const newFilePath = path.join(options.uploadDir, uniqueFileName);
    //*//    fs.renameSync(filePath, newFilePath);
    //*//  } else {
    //*//    // No file uploaded, use the inputted text
    //*//    uniqueFileName = fields.filename;
    //*//    actualFileName = 'text_input.txt';
    //*//     const textInput = fields.textInput;
    //*//    await saveText(textInput, uniqueFileName, options.uploadDir);
    //*//  }
    //*//  resolve({ uniqueFileName, actualFileName });
    //*// });
    //*// form.on('fileBegin', function (name, file) {
    //*//  file.path = path.join(options.uploadDir, file.name);
    });
  });
}

//*//async function saveText(text, uniqueFileName, uploadDir) {
//*//  const filePath = path.join(uploadDir, uniqueFileName);
//*//  await fs.promises.writeFile(filePath, text, 'utf-8');
//*//}

async function storeDataInIPFS(actualFileName) {
  // REPLACE with your own token: Signup/Login to web3.storage, Account->Create an API token
  const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDg0NTg5ZDI0Y2RFNDU0NkIxOUQyMjAyYjRhRTAxQkIwYTkzRjNFQTYiLCJpc3MiOiJ3ZWIzLXN0b3JhZ2UiLCJpYXQiOjE2ODU5MTI0NDE5NTQsIm5hbWUiOiJpcGZzLXRlc3QifQ.ukgMAPdf4BdV9AZPijBbHNEeR1d51mlq5hFiJGCVGYI'; // Replace with your actual Web3.Storage token
  const storage = new Web3Storage({ token });
  const uploadPath = path.join(process.cwd(), '/pages/uploads');
  const files = await getFilesFromPath(uploadPath, `/${actualFileName}`);
  const cid = await storage.put(files);
  const hash = cid.toString();
  return { cid: hash, message: `File uploaded to IPFS successfully. CID: ${hash}` };
}

// 1) move files from local PC to SERVER
// 2) store file in IPFS
async function handler(req, res) {
  try {
    const { uniqueFileName, actualFileName } = await moveFileToServer(req);
    console.log('..files are stored on the local server.');

    await new Promise(resolve => setTimeout(resolve, 2000)); // Remove this line if not necessary

    const response = await storeDataInIPFS(actualFileName);
    console.log('..file stored in IPFS. CID:', response.cid);

    return res.status(200).json(response);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: err.message });
  }
}

export default handler;
