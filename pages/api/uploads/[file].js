import fs from 'fs';
import path from 'path';

export default function handler(req, res) {
  const { file } = req.query;
  const filePath = path.join(process.cwd(), 'pages/uploads', file);

  fs.readFile(filePath, (err, data) => {
    if (err) {
      res.status(404).end();
      return;
    }

    if (file.endsWith('.txt')) {
      res.setHeader('Content-Type', 'text/plain');
      res.send(data);
    } else if (file.match(/\.(jpg|jpeg|png|gif)$/i)) {
      res.setHeader('Content-Type', 'image/jpeg'); // Modify the content type based on the image file type
      res.send(data);
    } else {
      res.status(404).end();
    }
  });
}
