// saves to IPFS via enabling Blockchain 
// !! EDIT pages/constant.js with your wallet info before running this

import {Web3Storage, getFilesFromPath } from 'web3.storage';
const {ethers} = require('ethers');
import * as Constants from "../constant";
import formidable from 'formidable';
import path from 'path';

export const config = {
    api: {
        bodyParser: false    // disable built-in body parser
    }
}

function moveFiletoServer(req) {
    return new Promise((resolve, reject) => {
        const options = {};
        options.uploadDir = path.join(process.cwd(), "/pages/uploads");
        options.filename = (name, ext, path, form) => {
            return path.originalFilename;
        }
        const form = formidable(options);

        form.parse(req, (err, fields, files) => {
            if (err) {
                console.error(err);
                reject("Something went wrong :/");
                return;
            }
            const uniqueFileName = fields.filename;
            const actualFileName = files.file.originalFilename;

            resolve({uniqueFileName, actualFileName});
        })
    })
}


async function storeDataInBlockchain(actualFileName, uniqueFileName) {
    const provider = new ethers.providers.JsonRpcProvider(Constants.API_URL);
    const signer = new ethers.Wallet(Constants.PRIVATE_KEY, provider);
    const StorageContract = new ethers.Contract(Constants.contractAddress, Constants.contractAbi, signer);

    const isStored = await StorageContract.isFileStored(uniqueFileName);

    console.log(isStored);

    if (isStored == false) {
        const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDg0NTg5ZDI0Y2RFNDU0NkIxOUQyMjAyYjRhRTAxQkIwYTkzRjNFQTYiLCJpc3MiOiJ3ZWIzLXN0b3JhZ2UiLCJpYXQiOjE2ODU5MTI0NDE5NTQsIm5hbWUiOiJpcGZzLXRlc3QifQ.ukgMAPdf4BdV9AZPijBbHNEeR1d51mlq5hFiJGCVGYI"; // Replace "YOUR_TOKEN_HERE" with your actual token
        const storage = new Web3Storage({token: token});
        const uploadPath = path.join(process.cwd(), "/pages/uploads");
        const files = await getFilesFromPath(uploadPath, `/${actualFileName}`);
        const cid = await storage.put(files);
        let hash = cid.toString();
        console.log("Loading..");
        const tx = await StorageContract.upload(uniqueFileName, hash);
        await tx.wait();
        const storedhash = await StorageContract.getIPFSHash(uniqueFileName);
        return {message: `IPFS hash is stored in the Smart Contract: ${storedhash}`}
    }

    else {
        console.log("..Data is already stored for this file name.");
        const IPFShash = await StorageContract.getIPFSHash(uniqueFileName);
        return {message: `IPFS hash is already stored in the Smart Contract: ${IPFShash}`}
    }
}

// 1) move files from local PC to SERVER
// 2) store file in IPFS
// 3) store IPFS hash with Blockchain
async function handler(req, res) {
    try {
        const {uniqueFileName, actualFileName} = await moveFiletoServer(req)
        console.log("..files are stored in the local server.");

        await new Promise(resolve => setTimeout(resolve, 2000));  //waiting for 2 seconds

        const response = await storeDataInBlockchain(actualFileName, uniqueFileName)
        console.log("..hash is stored in the Smart Contract.");

        return res.status(200).json(response);
    }
    catch (err) {
        console.error(err);
    }
}

export default handler;