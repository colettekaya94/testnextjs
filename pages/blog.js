import Link from "next/link";

export default function Blog(props) {
  return (
    <div className="relative h-screen">
      <div className="relative h-full">
        <div
          className="absolute inset-0 bg-cover bg-center bg-no-repeat"
          style={{
            backgroundImage: "url('/Hubble2.png')",
            filter: "brightness(44%)",
          }}
        ></div>
        <div className="relative flex flex-col items-center justify-center h-full">
          <h2 className="text-8xl font-bold text-pink-400 mb-2 text-center">Blog Posts</h2>
          <h3 className="text-4xl italic text-cyan-500 mb-4 text-center">{props.companyName}</h3>
          {props.posts.map((post, index) => {
            return (
              <div className="mb-4" key={index}>
                <h3 className="text-2xl mb-2 text-center">
                  <Link href={`/blog/${post.slug}`}>{post.title}</Link>
                </h3>
                <p className="mb-4">{post.content}</p>
                <hr />
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export async function getStaticProps() {
  const response = await fetch("https://kckaya.github.io/script.json")
  const data = await response.json()

  return {
    props: {
      posts: data.posts,
      companyName: "about DeSci"
    }
  }
}
