import Link from "next/link"
import { useRouter } from "next/router"
import styles from "../../styles/post.module.css"

export default function Post(props) {
  const router = useRouter()
  return (
    <>
      <p>
        <Link href="/">
          <small>&laquo; go back home</small>
        </Link>
      </p>
      <h2 className={styles.title} >{props.post.title}</h2>
      <p>{props.post.content}</p>
      <button className={styles.button} onClick={() => router.push("/blog")}>
        Return to the Future
      </button>
    </>
  )
}

export async function getStaticPaths() {
  const response = await fetch("https://kckaya.github.io/script.json")
  const data = await response.json()

  const thePaths = data.posts.map(pet => {
    return { params: { slug: pet.slug } }
  })

  return {
    paths: thePaths,
    fallback: false
  }
}

export async function getStaticProps(context) {
  const response = await fetch("https://kckaya.github.io/script.json")
  const data = await response.json()
  const thePost = data.posts.filter(post => post.slug === context.params.slug)[0]

  return {
    props: {
      post: thePost,
      title: thePost.title
    }
  }
}
