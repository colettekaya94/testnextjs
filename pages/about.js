import Link from 'next/link';

export default function About(props) {
  return (
    <div className="relative h-screen">
      <div
        className="absolute inset-0 bg-cover bg-center bg-no-repeat filter"
        style={{
          backgroundImage: "url('/Hubble1.png')",
          filter: "brightness(0.5)",
        }}
      ></div>
      <div className="flex flex-col items-center justify-center h-full relative z-10">
        <h2 className="text-5xl font-bold mb-8 text-orange-300">About Us</h2>
        <p className="text-3xl italic mb-8 text-indigo-400">
          Welcome to DeSci Labs!! We are a group of technology and data enthusiasts, building on a movement that integrates
          Science with Web3 technology. We intend to make academic papers more accessible by utilizing IPFS and Blockchain
          encryption technologies.
        </p>
        <p className="text-2xl mb-8 text-pink-300">We have {props.repoCount} public repos on GitHub.</p>
        <Link href="https://github.com/desci-labs" passHref legacyBehavior>
          <a className="text-orange-300 italic underline">Here is our Github repo..</a>
        </Link>
        <Link href="https://web3js.readthedocs.io/en/v1.10.0/" passHref legacyBehavior>
          <a className="text-cyan-600 italic underline">Here is the Web3.js documentation..</a>
        </Link>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const response = await fetch('https://api.github.com/users/desci-labs');
  const data = await response.json();

  return {
    props: {
      repoCount: data.public_repos,
    },
    revalidate: 8,
  };
}
