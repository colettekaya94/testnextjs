import { useEffect, useRef } from 'react';
import * as THREE from 'three';

const Earth = () => {
  const containerRef = useRef(null);
  const rendererRef = useRef(null);

  useEffect(() => {
    let container, renderer, scene, camera, earthMesh;

    const init = () => {
      container = containerRef.current;

      // Renderer
      renderer = new THREE.WebGLRenderer({ antialias: true });
      renderer.setSize(container.clientWidth, container.clientHeight);
      renderer.setPixelRatio(window.devicePixelRatio);
      container.appendChild(renderer.domElement);
      rendererRef.current = renderer;

      // Scene
      scene = new THREE.Scene();

      // Camera
      const fov = 45;
      const aspect = container.clientWidth / container.clientHeight;
      const near = 0.1;
      const far = 100;
      camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
      camera.position.z = 5; 

      // Earth Geometry
      const earthGeometry = new THREE.SphereGeometry(1.4, 32, 32); // Increase the size to 4
      const earthTexture = new THREE.TextureLoader().load('/planetEarth3.png');
      // planetEarth3.png reference: Earth Observatory/NASA, April 18 - October 23, 2012 <https://eoimages.gsfc.nasa.gov/images/imagerecords/79000/79765/dnb_land_ocean_ice.2012.3600x1800.jpg>
      const earthMaterial = new THREE.MeshBasicMaterial({ map: earthTexture });
      earthMesh = new THREE.Mesh(earthGeometry, earthMaterial);
      scene.add(earthMesh);

      // Event listeners
      container.addEventListener('mousemove', handleMouseMove);
      window.addEventListener('resize', handleResize);

      animate();
    };

    const handleMouseMove = (event) => {
      const mouse = new THREE.Vector2(
        (event.clientX / container.clientWidth) * 2 - 1,
        -(event.clientY / container.clientHeight) * 2 + 1
      );

      if (earthMesh) {
        const raycaster = new THREE.Raycaster();
        raycaster.setFromCamera(mouse, camera);

        const intersects = raycaster.intersectObject(earthMesh);
        const isHovered = intersects.length > 0;

        earthMesh.rotation.y += isHovered ? 0.02 : 0;
      }
    };

    const handleResize = () => {
      camera.aspect = container.clientWidth / container.clientHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(container.clientWidth, container.clientHeight);
    };

    const animate = () => {
      requestAnimationFrame(animate);

      // Rotate the Earth mesh
      if (earthMesh) {
        earthMesh.rotation.y += 0.01;
      }

      renderer.render(scene, camera);
    };

    init();

    return () => {
      container.removeEventListener('mousemove', handleMouseMove);
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div
      ref={containerRef}
      style={{
        width: '100vw', // Set the width to fill the entire viewport
        height: '100vh', // Set the height to fill the entire viewport
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      {/* WebGL Renderer will be injected here */}
    </div>
  );
};

export default Earth;
